### Install the the latest beta version of the Ionic CLI:

```sh
npm install -g ionic@beta
```

### Your system information:

```sh
Cordova CLI: 6.3.1
Gulp version:  CLI version 3.9.1
Gulp local:   Local version 3.9.1
Ionic Framework Version: 2.0.0-beta.11
Ionic CLI Version: 2.0.0-beta.37
Ionic App Lib Version: 2.0.0-beta.20
OS:
Node Version: v4.4.6

```


### Clone this repository:

```sh
git clone https://gitlab.com/AkiOkamoto/PWA-Development-Group-Japan.git
```

### Navigate to the PWA-Development-Group-Japan directory:

```sh
cd PWA-Development-Group-Japan
```

### Install the dependencies:

```sh
npm install
```

### Start the app in the browser:

```sh
ionic serve
```


From here, you can build and deploy the app on different platforms using the traditional Ionic commands (ionic build ios, etc.)

